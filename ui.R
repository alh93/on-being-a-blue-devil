ui <- fluidPage(
  
  title = "Trinity Enrollment, 2000",
  
  responsive = NULL,
  
  theme = shinythemes::shinytheme("flatly"),
  
  sidebarLayout(
    
    sidebarPanel(width = 3,
        sliderInput("num_colors",
                    label = "Number of colors:",
                    min = 1,
                    max = 9,
                    value = 7),
        # selectInput("term", "Choose Term", c("Fall" = "2000 Fall", "Spring" = "2000 Spring")),
        selectInput("school",
                    label = "Select School:",
                    choices = colnames(map_data_numeric),
                    selected = 1)
                 ),
    
    mainPanel(width = 9,
              tabsetPanel(
                tabPanel(title = 'Output Map', 
                          plotOutput(outputId = "map")),
                tabPanel(title = 'Data Table', 
                          dataTableOutput(outputId = 'table'))
                )
              ),
    
    position = "right",
    
    fluid = TRUE
    
  )
)
