install.packages("devtools")
devtools::install_github("thomasp85/gganimate")
devtools::install_github("thomasp85/transformr")
install.packages("readr")
install.packages("gifski")
install.packages("av")
install.packages("ggrepel")

library(readr)
library(ggplot2)
library(gganimate)
library(scales)
library(dplyr)
library(socviz)
library(ggrepel)
library(RColorBrewer)
library(ggthemes)


data_2000s <- read.csv("Aggregate_enrollment_data-cleaned2000s.csv")
state_data_2000s <-  data_2000s %>% 
      filter(state_or_country == "state", substring(term_descr, 6,10) == "Fall") %>%
      group_by(academic_year, home_loc) %>%
      dplyr::summarize(totalUndergrad = sum(trinity) + sum(ug_engineering), totalGraduate = sum(divinity) + sum(fuqua) + sum(graduate) + sum(law) + sum(nicholas_eos) + sum(medicine) + sum(pubpol)) %>%
      mutate(
        Region = case_when(
          home_loc %in% c("Maine", "Massachusetts", "Rhode Island", "Connecticut", "New Hampshire", "Vermont", "New York", "Pennsylvania", "New Jersey", "Delaware", "Maryland") ~ "Northeast",
          home_loc %in% c("West Virginia", "Virginia", "Kentucky", "Tennessee", "North Carolina", "South Carolina", "Georgia", "Alabama", "Mississippi", "Arkansas", "Louisiana", "Florida") ~ "Southeast",
          home_loc %in% c("Ohio", "Indiana", "Michigan", "Illinois", "Missouri", "Wisconsin", "Minnesota", "Iowa", "Kansas", "Nebraska", "South Dakota", "North Dakota") ~ "Midwest",
          home_loc %in% c("Texas", "Oklahoma", "New Mexico", "Arizona") ~ "Southwest",
          home_loc %in% c("Colorado", "Wyoming", "Montana", "Idaho", "Washington", "Oregon", "Utah", "Nevada", "California", "Alaska", "Hawaii") ~ "West",
          TRUE ~ "Other/Unknown"
          )
        )
state_data_2000s$academic_year <- as.numeric(state_data_2000s$academic_year)

# averagesByYear <- state_data_2000s %>% 
#   group_by(academic_year) %>%
#   summarize(avgX = mean(totalUndergrad), avgY = mean(totalGraduate))


p <- ggplot(data = state_data_2000s %>% filter(home_loc != "North Carolina"), mapping = aes(x = totalUndergrad, y = totalGraduate, size = totalUndergrad + totalGraduate, color = Region)) +
    geom_point() +
    theme_economist_white(gray_bg = FALSE) +
    scale_color_economist() +
    labs(x = "Total Undergraduate Enrollment", y = "Total Graduate Enrollment") +
    guides(size = FALSE) +
    theme_dark() +
    ggtitle("Duke Enrollment Among Non-NC States; Year: {frame_time}") +
    transition_time(academic_year) +
    ease_aes("linear") + 
    enter_fade() +
    exit_fade()

anim <- animate(p)

#anim_save("landingPageAnimation.gif", animation = anim)


