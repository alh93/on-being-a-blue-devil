library(readr)
library(ggplot2)
library(gganimate)
library(scales)
library(dplyr)
library(socviz)
library(ggrepel)
library(RColorBrewer)
library(ggthemes)

ubermega_totals <- ubermega %>%
  filter(Semester == "Fall", Sex == "All", Origin != "United States") %>%
  select(Year, Origin, All_Schools, Region)
  #Origin filter is to prevent double counting after 2000, when US is also counted as a country
colnames(ubermega_totals) <- c("Year", "Origin", "Total", "Region")

ubermega_males <- ubermega %>%
  filter(Semester == "Fall", Sex == "Male", Origin != "United States") %>%
  select(Year, Origin, All_Schools, Region)

colnames(ubermega_males) <- c("Year", "Origin", "Total_Male", "Region")

ubermega_females <- ubermega %>%
  filter(Semester == "Fall", Sex == "Female", Origin != "United States") %>%
  select(Year, Origin, All_Schools, Region)
colnames(ubermega_females) <- c("Year", "Origin", "Total_Female", "Region")

ubermega_sexes <- left_join(ubermega_females, ubermega_males)
ubermega_sexes <- ubermega_sexes %>% mutate(ratio = (1+Total_Female)/(1+Total_Male))

ubermega_stats <- left_join(ubermega_sexes, ubermega_totals)

# Bubble plot as follows:
# Bubble = State/Country
# Color = Region
# Size = Total Enrollment
# Frame = Year
# X-axis = Female / Male Ratio
# Y-axis = Country

isUS <- function(x) {x == "United States"}
ubermega_stats <- ubermega_stats %>% mutate(whereIsAt = isUS(Region))
ubermega_stats <- ubermega_stats %>%
  mutate(
  Region = case_when(
    Origin %in% c("Maine", "Massachusetts", "Rhode Island", "Connecticut", "New Hampshire", "Vermont", "New York", "Pennsylvania", "New Jersey", "Delaware", "Maryland") ~ "Northeast",
    Origin %in% c("West Virginia", "Virginia", "Kentucky", "Tennessee", "North Carolina", "South Carolina", "Georgia", "Alabama", "Mississippi", "Arkansas", "Louisiana", "Florida") ~ "Southeast",
    Origin %in% c("Ohio", "Indiana", "Michigan", "Illinois", "Missouri", "Wisconsin", "Minnesota", "Iowa", "Kansas", "Nebraska", "South Dakota", "North Dakota") ~ "Midwest",
    Origin %in% c("Texas", "Oklahoma", "New Mexico", "Arizona") ~ "Southwest",
    Origin %in% c("Colorado", "Wyoming", "Montana", "Idaho", "Washington", "Oregon", "Utah", "Nevada", "California", "Alaska", "Hawaii") ~ "West",
    TRUE ~ "Other/Unknown"
  )
)

p0 <- ggplot(data = ubermega_stats %>% filter(Year != 2020, Origin %nin% c("North Carolina",
                                                                           "Florida", "Georgia"),
                                              Region == "United States"), 
             mapping = aes(x = Year, y = Total, group = Origin))
p1 <- p0 +
  geom_line(alpha = .5) +
  geom_segment(aes(xend = 2015, yend = Total), linetype = 2, color = 'grey') + 
  geom_point(size = 2) + 
  geom_text(aes(x = 2015, label = Origin), hjust = 0) + 
  ggtitle("Non-NC State Enrollment, 1970-2020") +
  transition_reveal(Year) +
  theme_minimal()
anim <- animate(p1)
anim_save("finalLandingPlot2.gif", animation = anim)

p3 <- ggplot(data = ubermega_stats %>% filter(Year != 2020,
                                              Region != "United States"), 
             mapping = aes(x = Year, y = Total, group = Origin))
p4 <- p0 +
  geom_line(alpha = .5) +
  geom_segment(aes(xend = 2021, yend = Total), linetype = 2, color = 'grey') + 
  geom_point(size = 2) + 
  geom_text(aes(x = 2021, label = Origin), hjust = 0) + 
  facet_wrap(~ whereIsAt) +
  transition_reveal(Year) +
  theme_minimal()
animate(p1)

ubermegaStates <- ubermega %>%
  filter(Region == "United States") %>%
  mutate(section = case_when(
    Origin %in% c("Maine", "Massachusetts", "Rhode Island", "Connecticut", "New Hampshire", "Vermont", "New York", "Pennsylvania", "New Jersey", "Delaware", "Maryland") ~ "Northeast",
    Origin %in% c("West Virginia", "Virginia", "Kentucky", "Tennessee", "North Carolina", "South Carolina", "Georgia", "Alabama", "Mississippi", "Arkansas", "Louisiana", "Florida") ~ "Southeast",
    Origin %in% c("Ohio", "Indiana", "Michigan", "Illinois", "Missouri", "Wisconsin", "Minnesota", "Iowa", "Kansas", "Nebraska", "South Dakota", "North Dakota") ~ "Midwest",
    Origin %in% c("Texas", "Oklahoma", "New Mexico", "Arizona") ~ "Southwest",
    Origin %in% c("Colorado", "Wyoming", "Montana", "Idaho", "Washington", "Oregon", "Utah", "Nevada", "California", "Alaska", "Hawaii") ~ "West",
    TRUE ~ "Other/Unknown"
  ))



p5 <- ggplot(data = ubermegaStates %>% filter(Sex == "All", 
                                        Semester == "Fall",
                                        Region != "United States"),
             mapping = aes(x = Origin, y = All_Schools, fill = Region))
p6 <- p5 +
  geom_col() +
  theme_minimal() +
  coord_flip() +
  ggtitle("Duke Enrollment Internationally") +
  transition_reveal(Year) +
  ease_aes("linear") +
  enter_fade() +
  exit_fade()

 
anim1 <- animate(p6)









