﻿This BLUEDEVILBREAKDOWNreadme.txt file was generated on 2020-08-05 by Michaela Kotarba




GENERAL INFORMATION


1. Title of Dataset: Blue Devil Breakdown


2. Author Information
        A. Undergraduate Researcher Contact Information
                Name: Michaela Kotarba
                Institution: Duke University
                Email: michaela.kotarba@duke.edu


        B. Undergraduate Researcher Contact Information
                Name: Alex Burgin
                Institution: Duke University
                Email: alexander.burgin@duke.edu


        C. Undergraduate Researcher Contact Information
                Name: Kat Cottrell
                Institution: Duke University
                Email: katherine.cottrell@duke.edu


        D. Graduate Researcher Contact Information
                Name: Anna Holleman
                Institution: Duke University
                Email: anna.holleman@duke.edu


3. Date of data collection: 2020-05-26 through 2020-07-31 


4. Geographic location of data collection: Durham, NC


5. Thank you to the Duke Data+ program and the Rhodes Information Initiative for funding and supporting this project!




SHARING/ACCESS INFORMATION


1. Licenses/restrictions placed on the data: Data use for this project has been approved by the Duke University Registrar and Duke OIT. Future projects publishing the data should seek approval from these entities regarding the privacy of individuals within the student body and the security of the University.


2. Links to publications that cite or use the data: https://sites.duke.edu/bluedevilbreakdown


3. Was data derived from another source? Yes
1. Historical records provided by the Duke University Archives
2. Digitized enrollment data from 2000-2020 provided by the Duke University Registrar’s Office






DATA & FILE OVERVIEW


1. File List: 
UberMegaMaster_70S-20F-V4.csv — Enrollment data cleaned from 1970 Spring to 2020 Fall.
* Largest and most comprehensive file of data, containing all previously collected and cleaned data. Henceforth referred to as “UberMegaMaster” for this document.
Aggregate_enrollment_data-cleaned2000s.csv — Enrollment data from 2000 Spring - 2020 Fall, formatted to match the variables we have chosen for the Megamaster.
NinetiesMegamaster-V8.csv — Enrollment data cleaned from 1990 Spring to 1999 Spring.
Megamaster-Eighties.csv — Enrollment data cleaned from 1980 Spring to 1989 Fall.
Megamaster_70S-79F-V5.csv — Enrollment data cleaned from 1970 Spring to 1979 Fall.


1970-1971_cleaned.csv — Enrollment data cleaned from 1970 Fall to 1971 Spring.
1971-1972_cleaned.csv — Enrollment data cleaned from 1971 Fall to 1972 Spring.
1972-1973_cleaned.csv — Enrollment data cleaned from 1972 Fall to 1973 Spring
1973-1974_cleaned.csv — Enrollment data cleaned from 1973 Fall to 1974 Spring..
1974-1975_cleaned.csv — Enrollment data cleaned from 1974 Fall to 1975 Spring.
1975-1976_cleaned.csv — Enrollment data cleaned from 1975 Fall to 1976 Spring.
1976-1977_cleaned.csv — Enrollment data cleaned from 1976 Fall to 1977 Spring.
1977-1978_cleaned.csv — Enrollment data cleaned from 1977 Fall to 1978 Spring.
1978-1979_cleaned.csv — Enrollment data cleaned from 1978 Fall to 1979 Spring.
1979-1980_cleaned.csv — Enrollment data cleaned from 1979 Fall to 1980 Spring.
1980-1981_cleaned.csv — Enrollment data cleaned from 1980 Fall to 1981 Spring.
1981-1982_cleaned.csv — Enrollment data cleaned from 1981 Fall to 1982 Spring.
1982-1983_cleaned.csv — Enrollment data cleaned from 1982 Fall to 1983 Spring.
1983-1984_cleaned.csv — Enrollment data cleaned from 1983 Fall to 1984 Spring.
1984-1985_cleaned.csv — Enrollment data cleaned from 1984 Fall to 1985 Spring.
1985-1986_cleaned.csv — Enrollment data cleaned from 1985 Fall to 1986 Spring.
1986-1987_cleaned.csv — Enrollment data cleaned from 1986 Fall to 1987 Spring.
1987-1988_cleaned.csv — Enrollment data cleaned from 1987 Fall to 1988 Spring.
1988-1989_cleaned.csv — Enrollment data cleaned from 1988 Fall to 1989 Spring.
1989-1990_cleaned.csv — Enrollment data cleaned from 1989 Fall to 1990 Spring.
1990-1991_cleaned.csv — Enrollment data cleaned from 1990 Fall to 1991 Spring.
1991-1992_cleaned.csv — Enrollment data cleaned from 1991 Fall to 1992 Spring.
1992-1993_cleaned.csv — Enrollment data cleaned from 1992 Fall to 1993 Spring.
1993-1994_cleaned.csv — Enrollment data cleaned from 1993 Fall to 1994 Spring.
1994-1995_cleaned.csv — Enrollment data cleaned from 1994 Fall to 1995 Spring.
1995-1996_cleaned.csv — Enrollment data cleaned from 1995 Fall to 1996 Spring.
1996-1997_cleaned.csv — Enrollment data cleaned from 1996 Fall to 1997 Spring.
1997-1998_cleaned.csv — Enrollment data cleaned from 1997 Fall to 1998 Spring.
1998-1999_cleaned.csv — Enrollment data cleaned from 1998 Fall to 1999 Spring.


global.R - R script containing the global dataset (ubermega) as well as global variables referring to lines of text, facts, paragraphs, and images to be used later.


ui.R - R script containing input functions to receive decisions made by the user as well as still images from the computer and sections of text and timelines.


server.R - R script which converts user input into interactive outputs, such as the domestic and international mapping tools, data tables, and plots.


2. Relationship between files, if important: 
* UberMegaMaster_70S-20F-V4.csv is the final version of the compilation of Aggregate_enrollment_data-cleaned2000s.csv, NinetiesMegamaster-V8.csv, Megamaster-Eighties.csv, and Megamaster_70S-79F-V5.csv.
* Each Megamaster file (70s, 80s, and 90s) is the compilation of enrollment data for that respective decade. For example, Megamaster-Eighties.csv holds 1980-1981_cleaned.csv, 1981-1982_cleaned.csv, etc through 1989-1990.csv.


3. Are there multiple versions of the dataset? Yes
1. UberMegaMaster_70S-20F-V4.csv
   1. V4 indicates that this was the fourth version of the UberMegamaster. Four updates were made in order to correct inaccuracies (such as misplaced zero-values for countries which showed significant enrollment) and country name conflicts (such as Georgia the state vs. Georgia the country).


2. Megamaster_70S-79F-V5.csv
   1. Similarly to above, the V5 indicates that this was the fifth version of the 70s decade. Five updates were made in order to correct inaccuracies related to country names and enrollment values.






METHODOLOGICAL INFORMATION


1. Description of methods used for collection/generation of data: 
Enrollment records from the Duke registrar for the years 1970-1999 were collected by Valerie Gillespie from the Duke University Archives in the Rubenstein Library on 2020-05-26 and 2020-06-24. These were scanned into PDF format and uploaded for use by the undergraduate researchers.


Data from 2000-2020 was provided by the current registrar in an Excel file.


2. Methods for processing the data: 
Once uploaded, the PDF scans were cleaned and formatted by Burgin, Kotarba, and Cottrell, who used OCR software to convert the typewritten documents into Excel spreadsheets. Then, these spreadsheets had to be cleaned and checked for accurate enrollment numbers. Once scanned for accuracy and compared to original registrar records, these Excel sheets were saved as CSV files for each individual year.


The individual years were first compiled into decades by Cottrell, who then combined each decade (including the already-digital 2000-2020 data) into the UberMegaMaster, the collection of data for all programs and all years.


<describe how the submitted data were generated from the raw or collected data>


3. Instrument- or software-specific information needed to interpret the data: 
1. AbleToExtract (paid) - OCR software used to create machine-readable excel files from PDF scans of typewritten documents.




4. People involved with sample collection, processing, analysis and/or submission: 
1. Valerie Gillespie, Duke University Archivist, collected archived historical records, scanned each record to digitize by year, and shared the PDF documents with the undergraduate researchers.




DATA-SPECIFIC INFORMATION FOR: [UberMegaMaster_70S-20F-V4.csv]
<repeat this section for each dataset, folder or file, as appropriate>


1. Number of variables: 19


2. Number of cases/rows: 83325


3. Variable List: 


Variable
	Description
	Unit
	Year
	Calendar year for respective semester.
	Numeric calendar year (XXXX)
	Semester
	Semester in which students were enrolled
	Fall, Spring, or Other
	Origin
	Where students from this region applied from
	Geographical region (State or Country)
	Sex
	Biological sex provided by University/student reported
	Sex
	Trinity
	(Sex) students enrolled in Trinity program from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Nursing
	(Sex) students enrolled in undergraduate nursing program from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Graduate
	(Sex) students enrolled in graduate programs from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Divinity
	(Sex) students enrolled in divinity school from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Law
	(Sex) students enrolled in law school from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Business
	(Sex) students enrolled in graduate business program from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Engineering
	(Sex) students enrolled in undergraduate engineering from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Environment
	(Sex) students enrolled in school of the environment from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Medicine
	(Sex) students enrolled in medical school from (Origin) in (Semester) of (Year)
	Number of students enrolled
	All_Schools
	Total of (Sex) students enrolled in all programs from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Region
	Notes the continent/overall region a student is from according to the home state or country listed.
	Geographical region (continent or general area)
	All_UG
	Total of (Sex) students enrolled in all undergraduate schools from (Origin) in (Semester) of (Year)
	Number of students enrolled
	All_Grad
	Total of (Sex) students enrolled in graduate or professional programs from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Graduate.Nursing
	(Sex) students enrolled in graduate nursing from (Origin) in (Semester) of (Year)
	Number of students enrolled
	Allied.Health
	(Sex) students enrolled in Allied Health program from (Origin) in (Semester) of (Year)
	Number of students enrolled
	

4. Individual years and decades follow this centralized format, indicating raw enrollment numbers for each respective year, sex, semester, and origin by program. However, these individual years and decades are significantly smaller, containing a fraction of the cases/rows of the UberMegaMaster.